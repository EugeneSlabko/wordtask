<?php

class IndicatorsService
{
    private const KB_IN_BYTES = 1024;
    private const MB_IN_KILOBYTES = self::KB_IN_BYTES * 1024;
    private float $timeStart;
    private float $timeEnd;
    private float $memoryStart;
    private float $memoryEnd;

    public function startIndicate(): void
    {
        $this->timeStart = microtime(true);
        $this->memoryStart = memory_get_usage();
    }

    public function stopIndicate(): void
    {
        $this->timeEnd = microtime(true);
        $this->memoryEnd = memory_get_usage();
    }

    public function getResults(): IndicatorResults
    {
        return new IndicatorResults(
            timeUsed: $this->calculateTime(),
            memoryUsed: $this->calculateMemory(),
            peakMemoryUsed: $this->getPeakMemoryInMb()
        );
    }

    private function getPeakMemoryInMb(): int
    {
        $memoryPeakUsage = memory_get_peak_usage();
        $usedMemoryInMb = $memoryPeakUsage / self::MB_IN_KILOBYTES;

        return round($usedMemoryInMb);
    }

    private function calculateTime(): float
    {
        return $this->timeEnd - $this->timeStart;
    }

    private function calculateMemory(): int
    {
        $usedMemory = $this->memoryEnd - $this->memoryStart;
        $memoryInMb = $usedMemory;

        return round($memoryInMb);
    }
}
