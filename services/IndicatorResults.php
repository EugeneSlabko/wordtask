<?php

class IndicatorResults
{
    public function __construct(
        public float $timeUsed,
        public int $memoryUsed,
        public int $peakMemoryUsed
    ) {
    }
}
