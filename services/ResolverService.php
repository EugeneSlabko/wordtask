<?php

class ResolverService
{
    private const FILENAME = 'words.txt';

    private array $result = [];

    public function __construct(
        private readonly AlgorithmService $algorithmService,
        private readonly IndicatorsService $indicatorsService,
        private readonly PrintService $printService
    ) {
    }

    public function resolve(): void
    {
        $inputArray = $this->getInputArray();

        $this->start();
        $this->resolveAlgo($inputArray);
        $this->finish();
    }

    private function getInputArray(): array
    {
        $fileStr = file_get_contents(self::FILENAME);

        return explode(',', $fileStr);
    }

    private function resolveAlgo(array $inputArray): void
    {
        $resultArray = $this->algorithmService->resolveAlgo($inputArray);

        $this->setResult($resultArray);
    }

    private function setResult(array $result): void
    {
        $this->result = $result;
    }

    private function start(): void
    {
        $this->printService->printStart();
        $this->indicatorsService->startIndicate();
    }

    private function finish(): void
    {
        $this->indicatorsService->stopIndicate();

        $outputResult = $this->getOutputResults();
        $this->printService->printResults($outputResult);
    }

    private function getOutputResults(): OutputResults
    {
        return new OutputResults(
            indicatorResults: $this->indicatorsService->getResults(),
            resultArray: $this->result
        );
    }
}
