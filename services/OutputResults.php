<?php

class OutputResults
{
    public function __construct(
        public IndicatorResults $indicatorResults,
        public array $resultArray,
    ) {
    }
}
