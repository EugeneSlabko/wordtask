<?php

class AlgorithmService
{
    public function resolveAlgo(array $inputArray): array
    {
        $lettersArray = array_map('str_split', $inputArray);
        $countLettersArray = array_map('array_count_values', $lettersArray);

        $intersectedLettersWithCount = array_intersect_key(...$countLettersArray);
        $intersectedLetters = array_keys($intersectedLettersWithCount);

        $outArray = [];
        foreach ($intersectedLetters as $letter) {
            $selectedLetterInWordTimes = array_column($countLettersArray, $letter);
            $minimumIntersectedTimes = min($selectedLetterInWordTimes);

            $letterArray = array_fill(0, $minimumIntersectedTimes, $letter);
            $outArray = array_merge($outArray, $letterArray);
        }

        return $outArray;
    }
}
