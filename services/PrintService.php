<?php

class PrintService
{
    public function printStart(): void
    {
        print_r('Start!');
        print_r(PHP_EOL);
        print_r(PHP_EOL);
    }

    public function printResults(OutputResults $outputResults): void
    {
        $outputResult = $this->getResult($outputResults);

        foreach ($outputResult as $item) {
            print_r($item);
            print_r(PHP_EOL);
        }
    }

    private function getResult(OutputResults $outputResults): array
    {
        $outputString = 'ResultArray: ';
        $result = $outputResults->resultArray;
        $executionTime = sprintf('Execution time: %f, sec.', $outputResults->indicatorResults->timeUsed);
        $executionMemory = sprintf('Execution memory: %d bytes.', $outputResults->indicatorResults->memoryUsed);
        $peakMemory = sprintf('Peak memory: %d mb.', $outputResults->indicatorResults->peakMemoryUsed);

        return [
            $outputString,
            $result,
            $executionTime,
            $executionMemory,
            $peakMemory,
        ];
    }
}
