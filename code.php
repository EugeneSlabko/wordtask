<?php

include 'services/IndicatorResults.php';
include 'services/IndicatorsService.php';
include 'services/ResolverService.php';
include 'services/PrintService.php';
include 'services/AlgorithmService.php';
include 'services/OutputResults.php';

$indicatorService = new IndicatorsService();
$printService = new PrintService();
$algorithmService = new AlgorithmService();
$resolver = new ResolverService($algorithmService, $indicatorService, $printService);

$resolver->resolve();
